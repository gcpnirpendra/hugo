+++
title = "Introduction to Infrastracture as a code"
weight ="7"
+++

**Infrastructure as Code (IaC)** is the managing and provisioning of infrastructure through code instead of through manual processes.


**Version control** is an important part of IaC, and your configuration files should be under source control just like any other software source code file. 

> Deploying your infrastructure as code also means that you can divide your infrastructure into modular components that can then be combined in different ways through automation.

#### Benefits:
- **Cost reduction**
- **Increase in speed of deployments**
- **Reduce errors** 
- **Improve infrastructure consistency**
- **Eliminate configuration drift**

#### These are a few popular choices:
- **Chef**
- **Puppet**
- **Red Hat Ansible Automation Platform**
- **Saltstack**
- **Terraform**
- **AWS CloudFormation**

{{% notice note %}}
Terraform is Best tool for codifying your infrstracture .It can be used with any cloud providers. 
{{% /notice %}}
