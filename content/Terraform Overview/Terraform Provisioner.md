+++
title = "Terraform Provisioner"
weight ="15"
+++




_nirpendra kumar

- **Development Hybrid Cloud** : Get development infrastructure ready in a few clicks
- **DevOps Tools** : Automate the whole project lifecycle with a set of ready to use DevOps tools
- **Workstation as Code** : Homogenize the project team workstations in a fully automated way
- **Development Marketplace** : Re-use components from our Group wide developers’ community

_Digital Accelerators_ are technological foundations to build and run digital solutions

- **Container Orchestration & Service Mesh** : Ready to use orchestration and service mesh solutions to run applications packaged in containers
- **Cloud Native Stack** : A proven set of documented software components to accelerate the build of software applications (both micro-service applications and monolithic applications).
- **API Management** : Documentation and reusable example projects to accelerate the integration of applications at the information system level.

{{% notice info %}}
In addition, **Platform Status** is providing a live status of the DEP assets and **DEP4U** can help you to choose the relevant assets for your project. 
{{% /notice %}}
