+++
title = "Terraform Syllabus"
pre = ""
post = ""
chapter = "true"
weight = "1"
+++


## Terrafrom Course Details
- **Total Classes:**  **10**
- **Total Hours:**  **20** 
#### Terraform Syllabus

 - **Introduction to Infrastracture as a code.**
 - **Getting Started with Terraform.**
 - **Terraform Basics.**
 - **Terraform Variables**.
 - **Terraform State.**
 - **Terraform Workspaces.**
 - **Terraform with AWS.**
 - **Terraform Provisioner.**
 - **Terraform Import, Tainting Resources and Deubugging.**
 - **Terraform Modules.**
 - **Terraform Functions and Conditional Expressions.**

