package com.soprasteria.dep.quiz.repository;

import com.soprasteria.dep.commons.mongodb.DepReactiveMongoRepository;
import com.soprasteria.dep.quiz.model.entity.Quiz;

public interface QuizRepository extends DepReactiveMongoRepository<Quiz> {

}
