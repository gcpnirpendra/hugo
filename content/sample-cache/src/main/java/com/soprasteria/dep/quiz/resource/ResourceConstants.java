package com.soprasteria.dep.quiz.resource;

import static com.soprasteria.dep.commons.api.ApiConstants.API;

final class ResourceConstants {

    static final String QUIZZES = API + "/quizzes";

    private ResourceConstants() {
    }
}
