package com.soprasteria.dep.quiz.resource;

import com.soprasteria.dep.commons.error.SbsExceptionDTO;
import com.soprasteria.dep.quiz.dao.QuizDao;
import com.soprasteria.dep.quiz.mapper.QuizMapper;
import com.soprasteria.dep.quiz.model.api.QuizApiDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.util.UriBuilder;

import java.util.List;

import static com.soprasteria.dep.commons.error.DepBusinessCode.INVALID_CONTENT;
import static com.soprasteria.dep.commons.error.DepBusinessCode.RESOURCE_ALREADY_EXIST;
import static com.soprasteria.dep.commons.page.RangeConstants.RANGE_PARAM;
import static com.soprasteria.dep.commons.test.ExceptionVerifiersKt.verifySbsExceptionDTO;
import static com.soprasteria.dep.quiz.TestDataDto.javaCollectionQuestionDto;
import static com.soprasteria.dep.quiz.TestDataDto.javaQuizDto;
import static com.soprasteria.dep.quiz.resource.ResourceConstants.QUIZZES;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.PARTIAL_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

// @WithMockUser is used here because at this point in the quickstart we haven't introduced security yet
// but normally all your endpoints should expect authentication.
// It is however strongly discouraged to use @WithMockUser since it causes disastrous performances.
@WithMockUser
@SpringBootTest
@AutoConfigureWebTestClient
@AutoConfigureRestDocs
class QuizResourceTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private QuizDao quizDao;

    @BeforeEach
    void setup() {
        quizDao.initTest();
    }

    @Test
    void findAll_shouldReturnAnArrayOfQuizzes() {
        webTestClient.get()
                     .uri(QUIZZES)
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT.value())
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBodyList(QuizApiDto.class)
                     .consumeWith(res -> {
                         List<QuizApiDto> dtos = res.getResponseBody();
                         List<QuizApiDto> expected = quizDao.findAll().stream().map(QuizMapper::toApi).collect(toList());
                         assertThat(dtos).hasSize(quizDao.count()).containsExactlyInAnyOrderElementsOf(expected);
                     });
    }

    @Test
    void findAll_shouldReturnSingleQuiz() {
        webTestClient.get()
                     .uri(b -> b.path(QUIZZES).queryParam(RANGE_PARAM, "0-0").build())
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT.value())
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBodyList(QuizApiDto.class)
                     .hasSize(1);
    }

    @Test
    void findAll_shouldReturnEmptyArray() {
        quizDao.deleteAll();
        webTestClient.get()
                     .uri(QUIZZES)
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT.value())
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBodyList(QuizApiDto.class)
                     .hasSize(0);
    }

    @Test
    void create_shouldCreateAnInitializedQuiz() {
        QuizApiDto dto = new QuizApiDto().setTitle("API").setQuestions(singletonList(javaCollectionQuestionDto()));
        webTestClient.post()
                     .uri(QUIZZES)
                     .bodyValue(dto)
                     .exchange()
                     .expectStatus().isCreated()
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(QuizApiDto.class)
                     .consumeWith(res -> {
                         QuizApiDto mapped = res.getResponseBody();
                         assertThat(mapped.getId()).isNotNull();
                         assertThat(mapped.getTitle()).isEqualTo(dto.getTitle());
                         assertThat(mapped.getQuestions()).containsExactlyInAnyOrderElementsOf(dto.getQuestions());
                         assertThat(mapped.getDuration()).isNotNull();
                     });
    }

    @Test
    void create_shouldFail_whenTitleAlreadyTaken() {
        QuizApiDto dto = new QuizApiDto()
                .setTitle(javaQuizDto().getTitle())
                .setQuestions(singletonList(javaCollectionQuestionDto()));
        webTestClient.post()
                     .uri(QUIZZES)
                     .bodyValue(dto)
                     .exchange()
                     .expectStatus().isEqualTo(CONFLICT.value())
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(res -> verifySbsExceptionDTO(res, RESOURCE_ALREADY_EXIST));
    }

    @Test
    void create_shouldFail_whenBlankTitle() {
        QuizApiDto dto = new QuizApiDto().setTitle("");
        webTestClient.post()
                     .uri(QUIZZES)
                     .bodyValue(dto)
                     .exchange()
                     .expectStatus().isBadRequest()
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(res -> verifySbsExceptionDTO(res, INVALID_CONTENT));
    }
}
