package com.soprasteria.dep.quiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static com.soprasteria.dep.commons.constants.DepConstants.DEP_BASE_PACKAGE;

@SpringBootApplication(scanBasePackages = DEP_BASE_PACKAGE, proxyBeanMethods = false)
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
