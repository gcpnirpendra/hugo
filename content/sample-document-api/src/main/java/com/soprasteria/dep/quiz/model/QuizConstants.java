package com.soprasteria.dep.quiz.model;

import com.soprasteria.dep.commons.security.model.DefaultTokenType;

public final class QuizConstants {

    public static final String ADMIN_TOKEN_TYPE = "admin";
    public static final String SUPER_USER_TOKEN_TYPE = "super_user";
    public static final String MY_CUSTOM_FIELD = "myCustomField";
    public static final DefaultTokenType ADMINISTRATOR = new DefaultTokenType(ADMIN_TOKEN_TYPE);
    public static final DefaultTokenType SUPER_USER = new DefaultTokenType(SUPER_USER_TOKEN_TYPE);
    public static final String QUIZ_AUTH_CONTEXT_VERSION = "2.0.0";
}
