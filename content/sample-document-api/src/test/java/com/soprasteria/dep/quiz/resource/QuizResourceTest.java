package com.soprasteria.dep.quiz.resource;

import com.soprasteria.dep.commons.error.SbsExceptionDTO;
import com.soprasteria.dep.commons.test.InMemoryDepContextManager;
import com.soprasteria.dep.quiz.TestConstants;
import com.soprasteria.dep.quiz.dao.QuizDao;
import com.soprasteria.dep.quiz.mapper.QuizMapper;
import com.soprasteria.dep.quiz.model.api.QuizApiDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.test.web.reactive.server.WebTestClient;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import java.util.List;

import static com.soprasteria.dep.commons.context.model.ContextConstants.BEARER_PREFIX;
import static com.soprasteria.dep.commons.error.DepBusinessCode.INVALID_CONTENT;
import static com.soprasteria.dep.commons.error.DepBusinessCode.RESOURCE_ALREADY_EXIST;
import static com.soprasteria.dep.commons.page.RangeConstants.RANGE_PARAM;
import static com.soprasteria.dep.commons.test.ExceptionVerifiersKt.verifySbsExceptionDTO;
import static com.soprasteria.dep.commons.test.TestConstants.DEFAULT_SUB;
import static com.soprasteria.dep.quiz.TestDataDto.javaCollectionQuestionDto;
import static com.soprasteria.dep.quiz.TestDataDto.javaQuizDto;
import static com.soprasteria.dep.quiz.resource.ResourceConstants.QUIZZES;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.PARTIAL_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static com.soprasteria.dep.quiz.mapper.QuizMapper.toApi;

@SpringBootTest
@AutoConfigureWebTestClient
@AutoConfigureRestDocs
class QuizResourceTest {

    private static final String DEFAULT_AUTHORIZATION_HEADER = BEARER_PREFIX + " " + DEFAULT_SUB;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private QuizDao quizDao;

    @Autowired
    private InMemoryDepContextManager depContextDetailsService;

    @BeforeEach
    void setup() {

        quizDao.initTest();
        depContextDetailsService.createDepContext(TestConstants.DEP_TECHNICAL_AUTH_BUSINESS_CONTEXT);
    }

    @Test
    void findAll_shouldReturnAnArrayOfQuizzes() {
        webTestClient.get()
                     .uri(QUIZZES)
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT.value())
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBodyList(QuizApiDto.class)
                     .consumeWith(res -> {
                         List<QuizApiDto> dtos = res.getResponseBody();
                         List<QuizApiDto> expected = quizDao.findAll().stream().map(QuizMapper::toApi).collect(toList());
                         assertThat(dtos).hasSize(quizDao.count()).containsExactlyInAnyOrderElementsOf(expected);
                     });
    }

    @Test
    void findAll_shouldReturnSingleQuiz() {
        webTestClient.get()
                     .uri(b -> b.path(QUIZZES).queryParam(RANGE_PARAM, "0-0").build())
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT.value())
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBodyList(QuizApiDto.class)
                     .hasSize(1);
    }

    @Test
    void findAll_shouldReturnEmptyArray() {
        quizDao.deleteAll();
        webTestClient.get()
                     .uri(QUIZZES)
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .exchange()
                     .expectStatus().isEqualTo(PARTIAL_CONTENT.value())
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBodyList(QuizApiDto.class)
                     .hasSize(0);
    }

    @Test
    void create_shouldCreateAnInitializedQuiz() {
        QuizApiDto dto = new QuizApiDto().setTitle("API").setQuestions(singletonList(javaCollectionQuestionDto()));
        webTestClient.post()
                     .uri(QUIZZES)
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .bodyValue(dto)
                     .exchange()
                     .expectStatus().isCreated()
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(QuizApiDto.class)
                     .consumeWith(res -> {
                         QuizApiDto mapped = res.getResponseBody();
                         assertThat(mapped.getId()).isNotNull();
                         assertThat(mapped.getTitle()).isEqualTo(dto.getTitle());
                         assertThat(mapped.getQuestions()).containsExactlyInAnyOrderElementsOf(dto.getQuestions());
                         assertThat(mapped.getDuration()).isNotNull();
                     });
    }

    @Test
    void create_shouldFail_whenTitleAlreadyTaken() {
        QuizApiDto dto = new QuizApiDto()
                .setTitle(javaQuizDto().getTitle())
                .setQuestions(singletonList(javaCollectionQuestionDto()));
        webTestClient.post()
                     .uri(QUIZZES)
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .bodyValue(dto)
                     .exchange()
                     .expectStatus().isEqualTo(CONFLICT.value())
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(res -> verifySbsExceptionDTO(res, RESOURCE_ALREADY_EXIST));
    }

    @Test
    void create_shouldFail_whenBlankTitle() {
        QuizApiDto dto = new QuizApiDto().setTitle("");
        webTestClient.post()
                     .uri(QUIZZES)
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .bodyValue(dto)
                     .exchange()
                     .expectStatus().isBadRequest()
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(SbsExceptionDTO.class)
                     .consumeWith(res -> verifySbsExceptionDTO(res, INVALID_CONTENT));
    }

    @Test
    void create_shouldSucceedForAdministrator() {
        QuizApiDto dto = new QuizApiDto().setTitle("API").setQuestions(singletonList(javaCollectionQuestionDto()));
        webTestClient.post()
                     .uri(QUIZZES)
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .bodyValue(dto)
                     .exchange()
                     .expectStatus().isCreated()
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(QuizApiDto.class);
    }

    @Test
    void deleteById_shouldSucceedForAnyAuthenticatedUser() {
        QuizApiDto quiz = toApi(quizDao.findAny());
        webTestClient.delete()
                     .uri(QUIZZES + "/{id}", quiz.getId())
                     .header(AUTHORIZATION, BEARER_PREFIX + " " + "toto")
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(QuizApiDto.class)
                     .isEqualTo(quiz);
    }

    @Test
    void deleteById_shouldFail_whenNotAuthenticated() {
        String id = toApi(quizDao.findAny()).getId();
        webTestClient.delete()
                     .uri(QUIZZES + "/{id}", id)
                     .exchange()
                     .expectStatus().isUnauthorized();
    }

    @Test
    void deleteById_shouldFailForNonAdministrator() {
        QuizApiDto quiz = toApi(quizDao.findAny());
        webTestClient.delete()
                     .uri(QUIZZES + "/{id}", quiz.getId())
                     .header(AUTHORIZATION, DEFAULT_AUTHORIZATION_HEADER)
                     .exchange()
                     .expectStatus().isForbidden()
                     .expectHeader().contentType(APPLICATION_JSON_VALUE)
                     .expectBody(AccessDeniedException.class);
    }
}
