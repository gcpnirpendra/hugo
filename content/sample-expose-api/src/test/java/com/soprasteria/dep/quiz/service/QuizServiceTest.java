package com.soprasteria.dep.quiz.service;

import com.soprasteria.dep.quiz.config.QuestionProperties;
import com.soprasteria.dep.quiz.dao.QuizDao;
import com.soprasteria.dep.quiz.model.entity.Question;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static com.soprasteria.dep.commons.error.DepBusinessCode.INVALID_CONTENT;
import static com.soprasteria.dep.commons.error.DepBusinessCode.RESOURCE_ALREADY_EXIST;
import static com.soprasteria.dep.commons.page.RangeConstants.DEFAULT_RANGE;
import static com.soprasteria.dep.commons.test.ExceptionVerifiersKt.expectSbsException;
import static com.soprasteria.dep.quiz.TestData.javaCollectionQuestion;
import static com.soprasteria.dep.quiz.TestData.javaQuiz;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static reactor.kotlin.test.StepVerifierExtensionsKt.test;

@SpringBootTest
class QuizServiceTest {

    @Autowired
    private QuizService quizService;

    @Autowired
    private QuizDao quizDao;

    @Autowired
    private QuestionProperties questionProperties;

    @BeforeEach
    void beforeEach() {
        quizDao.initTest();
    }

    @Test
    void findAll_shouldFindSome() {
        test(quizService.findAll(DEFAULT_RANGE))
                .expectSubscription()
                .expectNextCount(quizDao.count())
                .verifyComplete();
    }

    @Test
    void findAll_shouldNotFindAny() {
        quizDao.deleteAll();
        test(quizService.findAll(DEFAULT_RANGE))
                .expectSubscription()
                .verifyComplete();
    }

    @Test
    void create_shouldPersistOne() {
        Quiz quiz = new Quiz().setTitle("Advanced Java").setQuestions(Collections.singletonList(javaCollectionQuestion()));
        test(quizService.create(quiz))
                .expectSubscription()
                .consumeNextWith(q -> assertThat(quizDao.findById(q.getId())).isEqualTo(quiz))
                .verifyComplete();
    }

    @Test
    void create_shouldFailWhenAlreadyExists() {
        test(quizService.create(new Quiz().setTitle(javaQuiz().getTitle())))
                .expectSubscription()
                .consumeErrorWith(e -> expectSbsException(e, RESOURCE_ALREADY_EXIST))
                .verify();
    }

    @Test
    void create_shouldFailWhenTooManyQuestions() {
        List<Question> questions = IntStream.rangeClosed(0, questionProperties.getMax()).boxed()
                                            .map(i -> javaCollectionQuestion())
                                            .collect(toList());
        Quiz quiz = quizDao.findAny().setQuestions(questions);
        test(quizService.create(quiz))
                .expectSubscription()
                .consumeErrorWith(e -> expectSbsException(e, INVALID_CONTENT))
                .verify();
    }
}
