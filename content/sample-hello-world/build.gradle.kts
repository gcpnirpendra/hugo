import com.soprasteria.dep.*

depJavaApplication {
    apiVersion = 1
    usePropertiesCompletion = false
    useKapt = false
    useDok = false
    useDokDsl = false
}

dependencies {
    implementation(dep("api"))
    runtimeOnly(dep("monitor"))
    testImplementation(dep("test"))
}
