# Summary

* **Purpose :** <!--Purpose-->
Example project to illustrate, how to use DEP Cloud Native Stack to expose the REST APIs that can be deployed on a Kubernetes cluster.
<!--/Purpose-->

##### In this project 2 imgage pull secrets are required, 
1. **Innersource registry secret:** This secret is used to pull the project's image from innersource registry during the Helm chart deployment on Innershift. It is handled by project's helm chart. The secret is created by the dep-library during pipeline execution.
1. **Dockerhub  registry secret:** This secret is used pull mongodb image from Dockerhub registry during the deployment of the Helm chart on Innershift. And it can be shared by multiple deployments within the same Openshift project. to be able to share the secret and void adding unnecessary complexity, We have created this secret directly inside the openshift project. You can refer this [link](https://docs.openshift.com/container-platform/4.6/openshift_images/managing_images/using-image-pull-secrets.html) to create the secret.


* **Group :** DEP Cloud Native Stack

* **Owner :** matthieu.vincent@soprasteria.com

* **Main contributors (multi-value) :** <!--Contributors--> matthieu.vincent@soprasteria.com, emmanuelle.rouille@soprasteria.com, sumit.sharma@soprasteria.com, ajay.singh@soprasteria.com <!--/Contributors-->

* **Code quality automatically verified :** Y

* **Security automatically verified :** Y

* **Automatic unit testing :** Y

* **Automatic non regression test :** N

* **Generic :** N

* **Pipeline :** https://innersource.soprasteria.com/dep/cloud-native-stack/cloud-native-stack/coding-dojo/quiz/-/pipelines

* **Presentation documentation :** https://dep-docs.apps.ocp4.innershift.sodigital.io/cloud-native-stack/examples/quickstart-example/

* **User documentation :** https://dep-docs.apps.ocp4.innershift.sodigital.io/cloud-native-stack/examples/quickstart-example/
