package com.soprasteria.dep.quiz.model;

import com.soprasteria.dep.commons.context.model.TokenType;
import com.soprasteria.dep.commons.security.model.DepAuthContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public final class DepTechnicalAuthBusinessContext
        implements DepAuthContext {

    @NotNull
    private final String sub;

    @NotNull
    private final String clientId;

    @NotNull
    private final String version;

    @NotNull
    private final Map authorizations;

    @Nullable
    private final String regionalLanguageCode;

    @Nullable
    private final String userAgent;

    @NotNull
    private final TokenType tokenType;

    @NotNull
    private String token;

    @Nullable
    private String myCustomField;

    @Nullable
    private String requestId;

    @Nullable
    private String depRequestId;

    @Nullable
    private String depUserAgent;

    public DepTechnicalAuthBusinessContext(String token,
                                           String sub,
                                           String clientId,
                                           String version,
                                           Map authorizations,
                                           String regionalLanguageCode,
                                           String userAgent,
                                           TokenType tokenType,
                                           String myCustomField) {
        this.token = token;
        this.sub = sub;
        this.clientId = clientId;
        this.version = version;
        this.authorizations = authorizations;
        this.regionalLanguageCode = regionalLanguageCode;
        this.userAgent = userAgent;
        this.tokenType = tokenType;
        this.myCustomField = myCustomField;
    }

    public int hashCode() {
        int result = this.getVersion().hashCode();
        result = 31 * result + this.getSub().hashCode();
        result = 31 * result + this.getTokenType().hashCode();
        return result;
    }

    public boolean equals(@Nullable Object other) {
        if ((DepTechnicalAuthBusinessContext) this == other) {
            return true;
        } else if (!(other instanceof DepTechnicalAuthBusinessContext)) {
            return false;
        } else if (this.getVersion()
                       .equals(((DepTechnicalAuthBusinessContext) other).getVersion())) {
            return false;
        } else if (this.getSub()
                       .equals(((DepTechnicalAuthBusinessContext) other).getSub())) {
            return false;
        } else {
            return !(this.getTokenType()
                         .equals(((DepTechnicalAuthBusinessContext) other).getTokenType()));
        }
    }

    @NotNull
    public String toString() {
        return "DepTechnicalAuthBusinessContext(subjectType=" +
               this.getTokenType() + ", sub='" + this.getSub() +
               "', clientId='" + this.getClientId() + "', version='" +
               this.getVersion() + "')";
    }

    @NotNull
    public String getVersion() {
        return this.version;
    }

    @NotNull
    public Map getAuthorizations() {
        return this.authorizations;
    }

    @NotNull
    public String getClientId() {
        return this.clientId;
    }

    @NotNull
    public String getSub() {
        return this.sub;
    }

    @NotNull
    public TokenType getTokenType() {
        return this.tokenType;
    }

    @Nullable
    public String getRegionalLanguageCode() {
        return this.regionalLanguageCode;
    }

    @Nullable
    public String getUserAgent() {
        return this.userAgent;
    }

    @NotNull
    public String getBearerToken() {
        return DefaultImpls.getBearerToken(this);
    }

    @NotNull
    public String getToken() {
        return this.token;
    }

    @Override
    public void setToken(@NotNull String s) {
        this.token = s;
    }

    @Nullable
    public String getMyCustomField() {
        return myCustomField;
    }

    public void setMyCustomField(@Nullable String myCustomField) {
        this.myCustomField = myCustomField;
    }

    @Nullable
    @Override
    public String getDepRequestId() {
        return depRequestId;
    }

    @Nullable
    @Override
    public String getDepUserAgent() {
        return depUserAgent;
    }

    @Nullable
    @Override
    public String getRequestId() {
        return requestId;
    }
}
