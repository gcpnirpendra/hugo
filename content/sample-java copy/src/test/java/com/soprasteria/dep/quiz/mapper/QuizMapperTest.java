package com.soprasteria.dep.quiz.mapper;

import com.soprasteria.dep.quiz.model.api.QuizApiDto;
import com.soprasteria.dep.quiz.model.entity.Question;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static com.soprasteria.dep.commons.mongodb.ObjectIdMappingsKt.toObjectId;
import static com.soprasteria.dep.quiz.TestData.javaQuiz;
import static com.soprasteria.dep.quiz.TestDataDto.javaQuizDto;
import static com.soprasteria.dep.quiz.mapper.QuizMapper.toApi;
import static com.soprasteria.dep.quiz.mapper.QuizMapper.toEntity;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class QuizMapperTest {

    @Test
    void toApi_shouldMap() {
        Quiz input = javaQuiz();
        QuizApiDto expected = toApi(input);
        assertThat(toObjectId(expected.getId())).isEqualTo(input.getId());
        assertThat(expected.getTitle()).isEqualTo(input.getTitle());
        assertThat(expected.getQuestions()).isNotEmpty();
        assertThat(expected.getDuration())
                .isEqualTo(Duration.ofSeconds(input.getQuestions().stream().mapToLong(Question::getDurationInSeconds).sum()));
    }

    @Test
    void toEntity_shouldMap() {
        QuizApiDto dto = javaQuizDto();
        Quiz expected = toEntity(dto);
        assertThat(expected.getId()).isEqualTo(toObjectId(dto.getId()));
        assertThat(expected.getTitle()).isEqualTo(dto.getTitle());
        assertThat(expected.getQuestions()).isNotEmpty();
    }
}
