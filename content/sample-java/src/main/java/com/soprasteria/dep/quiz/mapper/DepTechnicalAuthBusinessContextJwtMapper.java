package com.soprasteria.dep.quiz.mapper;

import com.nimbusds.jwt.JWTClaimsSet;
import com.soprasteria.dep.commons.context.model.ContextConstants;
import com.soprasteria.dep.commons.context.model.DepContextHeaders;
import com.soprasteria.dep.commons.context.model.TokenType;
import com.soprasteria.dep.commons.security.SecurityConstants;
import com.soprasteria.dep.commons.security.mappings.DepJwtMapper;
import com.soprasteria.dep.commons.security.model.DefaultTokenType;
import com.soprasteria.dep.quiz.model.DepTechnicalAuthBusinessContext;
import com.soprasteria.dep.quiz.model.QuizConstants;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import com.soprasteria.dep.commons.security.mappings.JwtMappingsKt;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Maps a JWT into a [DepTechnicalAuthBusinessContext]
 *
 * @see DepJwtMapper
 */
@Component
public class DepTechnicalAuthBusinessContextJwtMapper
        implements DepJwtMapper<DepTechnicalAuthBusinessContext> {

    public List<TokenType> supportedTypes;

    public List<String> supportedVersions;

    public DepTechnicalAuthBusinessContextJwtMapper() {
        this.supportedTypes = Arrays.asList(QuizConstants.ADMINISTRATOR, QuizConstants.SUPER_USER);
        this.supportedVersions = Arrays.asList(SecurityConstants.DEFAULT_JWT_VERSION, QuizConstants.QUIZ_AUTH_CONTEXT_VERSION);
    }

    @NotNull
    @Override
    public Iterable<TokenType> getSupportedTypes() {
        return supportedTypes;
    }

    @NotNull
    @Override
    public List<String> getSupportedVersions() {
        return supportedVersions;
    }

    @NotNull
    @Override
    public DepTechnicalAuthBusinessContext map(@NotNull String s,
                                               @NotNull JWTClaimsSet jwtClaimsSet,
                                               @NotNull DepContextHeaders depContextHeaders) {
        DepTechnicalAuthBusinessContext depTechnicalAuthBusinessContext =
                null;
        try {
            depTechnicalAuthBusinessContext =
                    new DepTechnicalAuthBusinessContext(s,
                                                        jwtClaimsSet.getStringClaim(
                                                                ContextConstants.SUB),
                                                        jwtClaimsSet.getStringClaim(
                                                                ContextConstants.CLIENT_ID),
                                                        JwtMappingsKt.version(jwtClaimsSet, SecurityConstants.DEFAULT_JWT_VERSION, supportedVersions),
                                                        Collections.EMPTY_MAP,
                                                        depContextHeaders.getRegionalLanguageCode(),
                                                        depContextHeaders.getUserAgent(),
                                                        new DefaultTokenType(jwtClaimsSet.getStringClaim(ContextConstants.TOKEN_TYPE)),
                                                        jwtClaimsSet.getStringClaim(QuizConstants.MY_CUSTOM_FIELD));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return depTechnicalAuthBusinessContext;
    }
}
