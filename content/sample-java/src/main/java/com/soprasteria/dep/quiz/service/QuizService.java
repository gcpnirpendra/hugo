package com.soprasteria.dep.quiz.service;

import com.soprasteria.dep.commons.error.DepBusinessCode;
import com.soprasteria.dep.commons.error.DepException;
import com.soprasteria.dep.commons.error.DepFeedback;
import com.soprasteria.dep.commons.mongodb.error.MongoErrorMappingsKt;
import com.soprasteria.dep.commons.page.Range;
import com.soprasteria.dep.quiz.config.QuestionProperties;
import com.soprasteria.dep.quiz.model.DepTechnicalAuthBusinessContext;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import com.soprasteria.dep.quiz.repository.QuizRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.soprasteria.dep.commons.error.DepBusinessCode.RESOURCE_NOT_FOUND;
import static com.soprasteria.dep.commons.security.ReactiveSecurityContextKt.currentDepAuthContext;
import static com.soprasteria.dep.commons.security.ReactiveSecurityContextKt.currentDepAuthContextOfType;
import static java.lang.String.format;


@Service
public class QuizService {

    private static final Logger logger = LoggerFactory.getLogger(QuizService.class);

    private final QuizRepository quizRepository;

    private final QuestionProperties questionProperties;

    public QuizService(QuizRepository quizRepository, QuestionProperties questionProperties) {
        this.quizRepository = quizRepository;
        this.questionProperties = questionProperties;
    }

    public Flux<Quiz> findAll(Range range) {
        return quizRepository.findAll(range)
                             .doOnSubscribe(s -> logger.debug("Searching quizzes within {}", range))
                             .doOnComplete(() -> logger.debug("Quizzes within {} retrieved", range));
    }

    public Mono<Quiz> create(Quiz quiz) {
        return Mono.fromSupplier(() -> checkQuiz(quiz))
                   .flatMap(quizRepository::insert)
                   .doOnSubscribe(s -> logger.info("Creating {}", quiz))
                   .doOnNext(q -> logger.info("Created {}", q))
                   .onErrorMap(MongoErrorMappingsKt:: mapSbsMongoError)
                   .then(logOnCreated(quiz));
    }

    private Mono<Quiz> logOnCreated(Quiz quiz) {
        return currentDepAuthContext()
                .doOnNext(c -> logger.info("Quiz {} created by {}",
                                           quiz.getId(), c.getClientId()))
                .thenReturn(quiz);
    }

    private Quiz checkQuiz(Quiz quiz) {
        if (quiz.getQuestions().size() > questionProperties.getMax()) {
            throw tooManyQuestionsError(quiz);
        }
        return quiz;
    }

    private DepException tooManyQuestionsError(Quiz quiz) {
        String msg = format("Quiz %s has more questions than maximum authorized %s",
                            quiz.getTitle(), questionProperties.getMax());
        return new DepFeedback.Builder(msg, DepBusinessCode.INVALID_CONTENT, logger::debug)
                .withSource(format("%s.questions#size", Quiz.class.getSimpleName()))
                .build()
                .toException();
    }

    public Mono<Quiz> deleteById(ObjectId id) {
        return quizRepository.findById(id)
                             .switchIfEmpty(Mono.error(notFoundError(id)))
                             .flatMap(this::delete)
                             .doOnSubscribe(
                                     s -> logger.info("Deleting quiz {}",
                                                      id));

    }

    private Mono<Quiz> delete(Quiz quiz) {
        return Mono.justOrEmpty(quiz.getId())
                   .flatMap(quizRepository::deleteById)
                   .then(logOnDeleted(quiz));

    }

    private Mono<Quiz> logOnDeleted(Quiz quiz) {
        return currentDepAuthContextOfType(DepTechnicalAuthBusinessContext.class)
                .doOnNext(c -> logger.info("Quiz {} deleted by {} with custom field {}",
                                           quiz.getId(), c.getClientId(), c.getMyCustomField()))
                .thenReturn(quiz);
    }

    private DepException notFoundError(ObjectId id) {
        return new DepFeedback(format("Quiz %s not found", id),
                                  RESOURCE_NOT_FOUND,
                                  logger::info).toException();
    }
}
