import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method POST()
        url '/api/quizzes'
        headers {
            header(authorization(), $(p('Bearer toto'), c('Bearer HOST_USER')))
            header(contentType(), applicationJson())
        }
        body(title: "", questions: [])
    }
    response {
        status BAD_REQUEST()
        body(
                returnCode: 'ERROR',
                traceId: $(regex('[a-z0-9]{16}')),
                ticketId: $(regex('[A-Za-z0-9]{6,36}')),
                feedbacks: [[
                                    type      : 'BIZ',
                                    severity  : 'ERROR',
                                    code      : 'INVALID_CONTENT',
                                    label     : 'Invalid request payload',
                                    spanId    : $(regex('[a-z0-9]{16}')),
                                    origin    : $(regex('quiz')),
                                    source    : "title",
                                    parameters: [],
                                    internal  : [message: $(anyNonEmptyString())],
                            ]])
        headers {
            header(expires(), '0')
            header(pragma(), 'no-cache')
            header('X-Frame-Options', 'DENY')
            header('Referrer-Policy', 'no-referrer')
            header('X-Content-Type-Options', 'nosniff')
            header('X-XSS-Protection', '1 ; mode=block')
            header(cacheControl(), 'no-cache, no-store, max-age=0, must-revalidate')
        }
    }
}
