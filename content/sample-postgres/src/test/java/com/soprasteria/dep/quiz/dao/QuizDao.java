package com.soprasteria.dep.quiz.dao;

import com.soprasteria.dep.commons.error.DepFeedback;
import com.soprasteria.dep.commons.test.ContractTestInitializer;
import com.soprasteria.dep.quiz.TestData;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import com.soprasteria.dep.quiz.repository.QuizRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.soprasteria.dep.commons.error.DepBusinessCode.RESOURCE_NOT_FOUND;
import static java.lang.String.format;

@Repository
public class QuizDao implements ContractTestInitializer {

    private QuizRepository quizRepository;
    private static final Logger logger = LoggerFactory.getLogger(QuizDao.class);

    QuizDao(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    @Transactional
    public List<Quiz> findAll() {
        return quizRepository.findAll();
    }

    public Quiz findById(String id) {
        return quizRepository.findById(id)
                .orElseThrow(() -> new DepFeedback(format("Quiz %s not found", id), RESOURCE_NOT_FOUND, logger::info).toException());

    }

    public Quiz findAny() {
        return quizRepository.findAll().get(0);
    }

    public void deleteAll() {
        quizRepository.deleteAll();
    }

    public long count() {
        return quizRepository.count();
    }

    @Override
    public void initTest() {
        quizRepository.deleteAll();
        quizRepository.save(TestData.createCoolQuiz());
        quizRepository.save(TestData.createBadQuiz());
    }

    @Override
    public void cleanUpTest() {}
}
