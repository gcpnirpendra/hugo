package com.soprasteria.dep.quiz.mapper;

import com.soprasteria.dep.quiz.model.api.QuestionApiDto;
import com.soprasteria.dep.quiz.model.entity.Question;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static com.soprasteria.dep.quiz.TestData.javaCollectionQuestion;
import static com.soprasteria.dep.quiz.TestDataDto.javaCollectionQuestionDto;
import static com.soprasteria.dep.quiz.mapper.QuestionMapper.toApi;
import static com.soprasteria.dep.quiz.mapper.QuestionMapper.toEntity;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class QuestionMapperTest {

    @Test
    void toApi_shouldMap() {
        Question input = javaCollectionQuestion();
        QuestionApiDto expected = toApi(input);
        assertThat(expected.getLabel()).isEqualTo(input.getLabel());
        assertThat(expected.getDuration()).isEqualTo(Duration.ofSeconds(input.getDurationInSeconds()));
        assertThat(expected.getAnswers()).isNotEmpty();
    }

    @Test
    void toEntity_shouldMap() {
        QuestionApiDto input = javaCollectionQuestionDto();
        Question expected = toEntity(input);
        assertThat(expected.getLabel()).isEqualTo(input.getLabel());
        assertThat(expected.getDurationInSeconds()).isEqualTo(input.getDuration().getSeconds());
        assertThat(expected.getAnswers()).isNotEmpty();
    }

    @Test
    void toEntity_shouldDefaultDurationTo30SecondsWhenNull() {
        QuestionApiDto input = javaCollectionQuestionDto().setDuration(null);
        Question expected = toEntity(input);
        assertThat(expected.getDurationInSeconds()).isEqualTo(30);
    }

    @Test
    void toEntityWithNullDuration_shouldDefaultTo30() {
        assertThat(toEntity(new QuestionApiDto()).getDurationInSeconds()).isEqualTo(30);
    }
}
