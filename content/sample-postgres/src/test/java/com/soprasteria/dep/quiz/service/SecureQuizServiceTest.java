package com.soprasteria.dep.quiz.service;

import com.soprasteria.dep.commons.error.DepException;
import com.soprasteria.dep.quiz.dao.QuizDao;
import com.soprasteria.dep.quiz.model.entity.Quiz;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.soprasteria.dep.commons.error.DepBusinessCode.RESOURCE_NOT_FOUND;
import static com.soprasteria.dep.commons.test.ExceptionVerifiersKt.expectSbsException;
import static com.soprasteria.dep.commons.test.SecurityTestExtensionsKt.testWithDepContext;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

// This test is separated from QuizServiceTest because our documentation is step by step and uses live code snippets.
// You MUST NEVER separate related domain logic is separated classes like this.
// If you follow the quickstart INCLUDE this method in QuizServiceTest class instead
@SpringBootTest
class SecureQuizServiceTest {

    @Autowired
    private SecureQuizService quizService;

    @Autowired
    private QuizDao quizDao;

    @BeforeEach
    void beforeEach() {
        quizDao.initTest();
    }

    @Test
    void deleteById_shouldSucceedForDepAdministrator() {
        Quiz quiz = quizDao.findAny();
        testWithDepContext(quizService.deleteById(quiz.getId()))
                .expectSubscription()
                .expectNext(quiz)
                .then(() -> assertThatExceptionOfType(DepException.class).isThrownBy(() -> quizDao.findById(quiz.getId())))
                .verifyComplete();
    }

    @Test
    void deleteById_shouldFailWhenNotFound() {
        testWithDepContext(quizService.deleteById("123"))
                .expectSubscription()
                .consumeErrorWith(e -> expectSbsException(e, RESOURCE_NOT_FOUND))
                .verify();
    }
}
