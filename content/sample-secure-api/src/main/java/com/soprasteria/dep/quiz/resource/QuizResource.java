package com.soprasteria.dep.quiz.resource;

import com.soprasteria.dep.commons.page.Range;
import com.soprasteria.dep.quiz.mapper.QuizMapper;
import com.soprasteria.dep.quiz.model.api.QuizApiDto;
import com.soprasteria.dep.quiz.service.QuizService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

import static com.soprasteria.dep.commons.api.pagination.RangeResponseKt.toResponse;
import static com.soprasteria.dep.quiz.resource.ResourceConstants.QUIZZES;

@RestController
@RequestMapping(QUIZZES)
class QuizResource {

    private final QuizService quizService;

    public QuizResource(QuizService quizService) {
        this.quizService = quizService;
    }

    @GetMapping
    public Mono<ResponseEntity<List<QuizApiDto>>> findAll(Range range) {
        return quizService.findAll(range)
                          .map(QuizMapper::toApi)
                          .as(f -> toResponse(f, QuizApiDto.class, range));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<QuizApiDto> create(@RequestBody @Valid QuizApiDto quiz) {
        return quizService.create(QuizMapper.toEntity(quiz))
                          .map(QuizMapper::toApi);
    }
}
