# How-to create `dep-font` from powerpoint diagrams

The idea here is to create a custom font on which DEP assets icons are linked to numbers:

* `1` : Hybrid Cloud Development asset
* `2` : Hybrid Cloud Production asset
* `3` : DevOps Tools asset
* etc.

## Pre-requisites

* Images available as diagrams in Powerpoint (or better as vectorial images, ex. SVG)
  * Actually they were extracted from a working Powerpoint `DEP in a nutshell.raw.pptx`, linked here, ask Gwénolé for the source
* Powerpoint installed
* InkScape installed (`choco install inkscape`)
* FontForge installed (`choco install fontforge`)

## Create the `dep-font`

If you've got some vectorial images for the icons, you can directly go to `Create the font` section.

### Convert images into vectorial images (SVG)

* Copy diagram from original powerpoint to a blank slide in Powerpoint (we use `dep-assets.pptx` in this directory to grab all the icons)
* Resize diagram so it fits the all slide,  without changing proportions (maintain `SHIFT` when resizing it)
* Save the diagram as a PNG image: `Right click > Save as image`
* Import the PNG image in InkScape: `File > Import`
* Select the object imported and vectorize it:
  * `Path > Vectorize a matricial object`
  * Use `Treshold` and try to find the best value with the overview
  * `Validate`
* Copy the new created object to a new file in InkScape
* Adjust size : `Edition > Adjust page size to selection`
* Save as SVG (InkScape SVG is OK) : `File > Save as...`

### Create the font

For each icon, you want to associate to a number (or a letter):

* Open FontForge and select `New Font`
* Doucle-click on letter you want to edit (for ex: `1`)
* `File > import`
* If you need to center vertically the glyph, just select all (`CTRL+A`) et move up or down using your keyboard arrows

Finally:

* save the font (as .sfd) to be able to edit it again
* generate the font, as Woff 2 format : `File > Generate Font > Generate` (just accept the errors if you've got some or fix them if you're brave ;))

### Use `dep-font` in website

* Import the font into your CSS, and declare a class to use it
N.B. : in the documentation, the CSS file used is `css/theme-dep.css`

```css
 @font-face {
    font-family: "dep-font";
    src: url("../dep-font2.woff2") format("woff2");
}

.dep-font {
    font-family: "dep-font";
    font-style: normal;
    font-size: 2em;
}
```

* Then use your CSS class to use your font

For expample, the following line will display the glyph associated to `9` letter.

```html
<i class='dep-font'>9</i>
```

Enjoy !

#### Usage example

See `dep-font2_test.html` file
